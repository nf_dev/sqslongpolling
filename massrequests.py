import requests
import grequests


INTERVAL = 600
LOWER_LIMIT = 1
UPPER_LIMIT = LOWER_LIMIT + INTERVAL


def sendmessagestoqueue():
    urls = []
    for msgid in range(LOWER_LIMIT, UPPER_LIMIT):

        reslongpollingurl = "http://localhost:3000/sendnormal?message=mymessage" + \
            str(msgid)
        resnormalurl = "http://localhost:3000/sendpolling?message=mymessage" + \
            str(msgid)

        # reslongpolling = requests.get()
        # resnormal = requests.get()
        # print(reslongpolling.status_code + str(msgid))
        # print(resnormal.status_code + str(msgid))
        urls.append(reslongpollingurl)
        urls.append(resnormalurl)

    rs = [grequests.get(u) for u in urls]
    print(grequests.map(rs))


def sendsmallsampletoqueue():
    urls = []
    for msgid in range(0, 20):

        reslongpollingurl = "http://localhost:3000/sendnormal?message=mymessage" + \
            str(msgid)
        resnormalurl = "http://localhost:3000/sendpolling?message=mymessage" + \
            str(msgid)

        reslongpolling = requests.get(reslongpollingurl)
        resnormal = requests.get(resnormalurl)
        print(reslongpolling.status_code + str(msgid))
        print(resnormal.status_code + str(msgid))


def receivemessages_fromqueue(queueurl, msgdeleteurl):
    urls = []

    for i in range(1, 20):
        res = requests.get(queueurl)
        print(res.status_code , i)
        receipt = res.json()["Messages"][0]["ReceiptHandle"]
        jsondata = {'receipt' : receipt}
        delreq = requests.post(msgdeleteurl, json=jsondata)
        print(delreq.status_code, receipt, i)

def receivemessages_fromqueue_concurrent(queueurl):
    urls = []
    for i in range(1, 30):
        urls.append(queueurl)
    
    rs = [grequests.get(u) for u in urls]
    print(grequests.map(rs))


def main():
    longpollingurl = 'http://localhost:3000/receivelongpolling'
    normalurl = 'http://localhost:3000/receivenormal'
    deleteurl = 'http://localhost:3000/delete'
    for i in range(15):
        receivemessages_fromqueue_concurrent(normalurl)
        receivemessages_fromqueue(normalurl, deleteurl)
        


if __name__ == "__main__":
    main()

ss = 3
