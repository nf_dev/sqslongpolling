// Require objects.
var express  = require('express');
var app      = express();
var aws      = require('aws-sdk');
var bodyParser = require('body-parser')
var queueUrlNormal = "https://sqs.ap-southeast-1.amazonaws.com/593693325525/sqsnormal2";
var queueUrlLongPolling = "https://sqs.ap-southeast-1.amazonaws.com/593693325525/sqslongpolling2";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

    
// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

// Creating a queue.
app.get('/create', function (req, res) {
    var queueName = req.query.queuename;
    var params = {
        QueueName: queueName
    };
    
    sqs.createQueue(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Listing our queues.
app.get('/list', function (req, res) {
    sqs.listQueues(function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Sending a message to multiple queues
// hardcoded logic
app.get('/sendnormal', function (req, res) {
    var msg = req.query.message;
    var params = {
        MessageBody: msg,
        QueueUrl: queueUrlNormal,
        DelaySeconds: 0
    };

    sqs.sendMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

app.get('/sendpolling', function (req, res) {
    //queue2
    var msg = req.query.message;
    var params = {
        MessageBody: msg,
        QueueUrl: queueUrlLongPolling,
        DelaySeconds: 0
    };
    sqs.sendMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Receive a message.
// NOTE: This is a great long polling example. You would want to perform
// this action on some sort of job server so that you can process these
// records. In this example I'm just showing you how to make the call.
// It will then put the message "in flight" and I won't be able to 
// reach that message again until that visibility timeout is done.
app.get('/receivelongpolling', function (req, res) {
    var params = {
        QueueUrl: queueUrlLongPolling,
        MaxNumberOfMessages: 1,
        VisibilityTimeout: 30 // 30 sec wait time for anyone else to process.
    };
    
    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

app.get('/receivenormal', function (req, res) {
    var params = {
        QueueUrl: queueUrlNormal,
        MaxNumberOfMessages: 1
    };
    
    sqs.receiveMessage(params, function (err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Deleting a message.
app.post('/delete', function (req, res) {

    var receipt = req.body.receipt;

    var params = {
        QueueUrl: queueUrlNormal,
        ReceiptHandle: String(receipt)
    };
    
    sqs.deleteMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Purging the entire queue.
app.get('/purge', function (req, res) {
    var params = {
        QueueUrl: queueUrlNormal
    };
    
    sqs.purgeQueue(params, function(err, data) {
        if(err) {
            res.send(err);
        } 
        else {
            res.send(data);
        } 
    });
});

// Start server.
var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('AWS SQS example app listening at http://%s:%s', host, port);
});
